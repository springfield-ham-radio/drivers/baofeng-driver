import { RadioProgram, RadioChannel, RadioProgrammedChannel, RadioTone, RadioToneType, RadioMemory } from '@springfield/ham-radio-api';
import { RadioMemorySegment, RadioSegmentedMemory } from '@springfield/ham-radio-driver-utils';
import { BaofengConfig } from './baofeng-config.js';
import { dcsByValue } from './baofeng-dcs-tones.js';
import { LogLayer } from 'loglayer';

export class BaofengDecoder {
  private config: BaofengConfig;
  private logger: LogLayer;

  constructor(config: BaofengConfig, logger: LogLayer) {
    this.config = config;
    this.logger = logger;
  }

  public decode(memory: RadioMemory): RadioProgram {
    const radioProgram: RadioProgram = {
      settings: {},
      channels: [],
    };

    const segmentedMemory: RadioSegmentedMemory = memory.contents;

    for (let channelNumber = 0; channelNumber < this.config.numberChannels; channelNumber++) {
      this.logger.debug(`Decoding channel: ${channelNumber}`);

      const channelAddress = this.getChannelAddress(channelNumber);
      const channelSegment = segmentedMemory.findSegment(channelAddress);

      if (channelSegment == undefined) {
        throw new Error(`Could not locate segment for channel: ${channelNumber}`);
      }

      const channelOffset = channelAddress - channelSegment.startAddress;

      if (channelSegment.data[channelOffset] != 0xff) {
        const channel = this.decodeChannel(memory, channelSegment, channelOffset, channelAddress, channelNumber);
        radioProgram.channels.push(channel);
      }
    }

    return radioProgram;
  }

  private getChannelAddress(channelNumber: number): number {
    return channelNumber * this.config.channelSize;
  }

  private decodeChannel(memory: RadioMemory, channelSegment: RadioMemorySegment, channelOffset: number, channelAddress: number, channelNumber: number): RadioProgrammedChannel {
    const transmitPower = this.decodePower(channelSegment.data[channelOffset + this.config.powerOffset]);

    const name = this.decodeChannelName(channelAddress, memory);
    const receiveFrequency = this.decodeFrequency(channelSegment, channelOffset, this.config.receiveFrequencyOffset);
    const transmitFrequency = this.decodeFrequency(channelSegment, channelOffset, this.config.transmitFrequencyOffset);
    const receiveTone = this.decodeTone(channelSegment, channelOffset, this.config.receiveToneOffset);
    const transmitTone = this.decodeTone(channelSegment, channelOffset, this.config.transmitToneOffset);

    const radioChannel: RadioChannel = { name, transmitFrequency, transmitTone, receiveFrequency, receiveTone };
    const radioSpecificChannelSettings = { transmitPower };
    return { channelNumber, radioChannel, settings: radioSpecificChannelSettings };
  }

  private decodePower(power: number) {
    return power == 0x0 ? 5 : 1;
  }

  private decodeChannelName(channelAddress: number, memory: RadioMemory) {
    const segmentedMemory: RadioSegmentedMemory = memory.contents;
    const channelNameAddress = 0x1000 + channelAddress;
    const segment = segmentedMemory.findSegment(channelNameAddress);

    if (segment == undefined) {
      throw new Error(`Could not locate segment at address: ${channelAddress.toString(16)}`);
    }

    const offset = channelNameAddress - segment.startAddress;

    let channelName = '';

    for (let i = 0; i < 7; i++) {
      const value = segment.data[offset + i];

      if (value != 0xff) channelName += String.fromCharCode(value);
    }

    return channelName;
  }

  private decodeFrequency(channelSegment: RadioMemorySegment, channelOffset: number, valueOffset: number) {
    let bcd = 0;

    for (let i = 0; i < 4; i++) {
      bcd |= channelSegment.data[channelOffset + valueOffset + i] << (8 * i);
    }

    return parseInt(bcd.toString(16), 10) * 10;
  }

  private decodeTone(channelSegment: RadioMemorySegment, channelOffset: number, valueOffset: number): RadioTone {
    const value = (channelSegment.data[channelOffset + valueOffset] & 0xff) | (channelSegment.data[channelOffset + valueOffset + 1] << 8);

    if (value < 105) {
      const dcs = dcsByValue.get(value);

      if (dcs == undefined) {
        throw new Error(`Could not decode DCS tone from data value: '${value}'`);
      }

      return { tone: dcs, type: RadioToneType.DCS };
    } else {
      return { tone: value / 10.0, type: RadioToneType.CTCSS };
    }
  }
}
