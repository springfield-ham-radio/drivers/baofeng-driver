import { RadioProgram, RadioMemory } from '@springfield/ham-radio-api';
import { RadioCodec } from '@springfield/ham-radio-api';
import { BaofengConfig } from './baofeng-config.js';
import { BaofengDecoder } from './baofeng-decoder.js';
import { BaofengEncoder } from './baofeng-encoder.js';
import { LogLayer } from 'loglayer';

export class BaofengCodec implements RadioCodec {
  private decoder;
  private encoder;

  constructor(radioModelId: string, config: BaofengConfig, logger: LogLayer) {
    this.decoder = new BaofengDecoder(config, logger);
    this.encoder = new BaofengEncoder(radioModelId, config, logger);
  }

  decode(memory: RadioMemory): RadioProgram {
    return this.decoder.decode(memory);
  }

  encode(program: RadioProgram): RadioMemory {
    return this.encoder.encode(program);
  }
}
