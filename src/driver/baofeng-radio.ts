import { RadioDriver, RadioModel, RadioSchema } from '@springfield/ham-radio-api';

export interface BaofengRadio {
  getSchema(): Promise<RadioSchema>;
  getModel(): RadioModel;
  getDriver(): RadioDriver;
}
