import { SerialPort } from 'serialport';
import { ByteLengthParser } from '@serialport/parser-byte-length';
import { RadioSegmentedMemoryDriver, RadioMemorySegment } from '@springfield/ham-radio-driver-utils';
import { formatSegment } from '@springfield/ham-radio-driver-utils';
import { BaofengCodec } from './baofeng-codec.js';
import { BaofengConfig } from './baofeng-config.js';
import { LogLayer } from 'loglayer';

export class BaofengDriver extends RadioSegmentedMemoryDriver {
  private config: BaofengConfig;
  private totalMemorySegments: number;
  private nextReadAddress: number;

  constructor(radioModelId: string, logger: LogLayer, config: BaofengConfig) {
    super(radioModelId, new BaofengCodec(radioModelId, config, logger), logger);
    this.config = config;
    const channelSegments = (config.channelMemorySegment.endAddress - config.channelMemorySegment.startAddress) / config.memorySegmentSize;
    const settingsSegments = (config.settingsMemorySegment.endAddress - config.settingsMemorySegment.startAddress) / config.memorySegmentSize;
    this.totalMemorySegments = channelSegments + settingsSegments;
    this.nextReadAddress = 0;
  }

  protected getNumberMemorySegments(): number {
    return this.totalMemorySegments;
  }

  protected async connectToRadio(path: string): Promise<SerialPort> {
    this.logger.debug(`Connecting to serial port: '${path}'`);
    const port = new SerialPort({ path, baudRate: 9600 });
    await this.sendMagicNumber(port);
    await this.getIdentifier(port);
    return port;
  }

  protected async sendMagicNumber(port: SerialPort): Promise<void> {
    this.logger.debug('sendMagicNumber()');

    return new Promise((resolve, reject) => {
      // Set up the serial port to receive data back from the radio

      const parser = port.pipe(new ByteLengthParser({ length: 1 }));

      // The radio is expected to return 0x06 after sending the magic number

      parser.on('data', (data) => {
        if (data[0] === 0x06) {
          this.logger.debug('Radio responded correctly to magic number');
          port.unpipe();
          resolve();
        } else {
          this.logger.debug(`Radio responded to magic number with unexpected value: '${data[0].toString(16)}'`);
          reject('Radio did not respond correctly to magic number');
        }
      });

      // We need to set up an error handler on the serial port in order to properly close the port and reject the promise.

      parser.on('error', (error) => {
        if (port && port.isOpen) {
          port.close();
        }

        this.logger.debug(`Unexpected error on serial port: '${error.message}'`);
        reject(error);
      });

      // Now that the serial port is set up, send the magic number to the radio to wake it up

      this.logger.debug(`Sending magic number: ${this.config.magicNumber}`);
      port.write(this.config.magicNumber);
    });
  }

  private getIdentifier(port: SerialPort): Promise<Uint8Array> {
    return new Promise((resolve, reject) => {
      const parser = port.pipe(new ByteLengthParser({ length: 8 }));

      parser.on('data', (data) => {
        this.logger.debug(`Recieved radio identifier: '${Buffer.from(data).toString('hex')}'`);
        resolve(data);
        port.unpipe();
      });

      parser.on('error', (error) => {
        this.logger.debug(`Failed to get radio identifier: ${error.message}`);
        reject(error);
      });

      // Send 0x02 to the radio to retrieve the identifier

      this.logger.debug(`Sending 0x02 to get radio identifier`);
      port.write([0x02]);
    });
  }

  protected async beginRadioRead(port: SerialPort): Promise<void> {
    this.nextReadAddress = this.config.channelMemorySegment.startAddress;
    await this.beginClone(port);
  }

  protected async endRadioRead(): Promise<void> {}

  private async beginClone(port: SerialPort): Promise<void> {
    return new Promise((resolve, reject) => {
      this.logger.debug('beginClone()');

      const parser = port.pipe(new ByteLengthParser({ length: 1 }));

      // The radio is expected to return 0x06 after sending start clone

      parser.on('data', (data) => {
        if (data[0] === 0x06) {
          port.unpipe();
          resolve();
        } else {
          reject('Failed to initiate clone');
        }
      });

      parser.on('error', (error) => reject(error));

      // Send 0x06 to the radio to start the clone (read memory)

      port.write([0x06]);
    });
  }

  protected async readSegment(port: SerialPort): Promise<RadioMemorySegment | null> {
    if (this.nextReadAddress === -1) {
      return null;
    }

    return new Promise((resolve, reject) => {
      // The radio is expected to respond with the character X followed by the address, length, and memory segment contents
      // The buffer must be 4 bytes larger than the segment size to hold the X, address, and length

      const parser = port.pipe(new ByteLengthParser({ length: this.config.memorySegmentSize + 4 }));

      parser.on('data', (data) => {
        if (data[0] === 'X'.charCodeAt(0)) {
          port.unpipe();
          const cleanupParser = port.pipe(new ByteLengthParser({ length: 1 }));

          // The radio is expected to respond with 0x06 after we acknowledge we have received the memory segment

          cleanupParser.on('data', (handshake) => {
            if (handshake[0] === 0x06) {
              port.unpipe();
              this.nextReadAddress += this.config.memorySegmentSize;

              if (this.nextReadAddress > this.config.channelMemorySegment.endAddress && this.nextReadAddress < this.config.settingsMemorySegment.startAddress) {
                this.nextReadAddress = this.config.settingsMemorySegment.startAddress;
              }

              if (this.nextReadAddress > this.config.settingsMemorySegment.endAddress) {
                this.nextReadAddress = -1;
              }

              resolve({ startAddress: this.nextReadAddress, length: this.config.memorySegmentSize, data: data.slice(4) });
            } else {
              reject(`Unexpected response from radio at end of segment: ${Buffer.from(data).toString('hex')}`);
            }
          });

          // Send 0x06 to the radio to acknowledge we have received the memory segment

          port.write([0x06]);
        } else {
          reject(`Unexpected response from radio: ${Buffer.from(data).toString('hex')}`);
        }
      });

      parser.on('error', (error) => reject(error));

      // Send the character S followed by the segment address and the number of bytes to the radio

      port.write(['S'.charCodeAt(0), this.nextReadAddress >> 8, this.nextReadAddress, this.config.memorySegmentSize]);
    });
  }

  protected async beginRadioWrite(_port: SerialPort): Promise<void> {}

  protected async endRadioWrite(_port: SerialPort): Promise<void> {}

  protected writeSegment(port: SerialPort, segment: RadioMemorySegment): Promise<void> {
    return new Promise((resolve, reject) => {
      this.logger.debug(formatSegment(segment));
      const parser = port.pipe(new ByteLengthParser({ length: 1 }));

      // The radio is expected to return 0x06 after sending the segment

      parser.on('data', (data) => {
        if (data[0] === 0x06) {
          port.unpipe();
          resolve();
        } else {
          reject(`Unexpected response from radio at end of segment: ${Buffer.from(data).toString('hex')}`);
        }
      });

      parser.on('error', (error) => reject(error));

      // Send the character X followed by the segment address and the number of bytes to write to the radio

      // port.write(['X'.charCodeAt(0), segment.startAddress >> 8, segment.startAddress, this.config.memorySegmentSize]);

      // Send the data contents to the radio

      // port.write(Array.from(segment.data));
    });
  }
}
