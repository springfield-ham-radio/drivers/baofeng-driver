interface BaofengMemorySegmentConfig {
  startAddress: number;
  endAddress: number;
}

export interface BaofengConfig {
  channelMemorySegment: BaofengMemorySegmentConfig;
  settingsMemorySegment: BaofengMemorySegmentConfig;
  memorySegmentSize: number;
  magicNumber: number[];
  receiveFrequencyOffset: number;
  transmitFrequencyOffset: number;
  receiveToneOffset: number;
  transmitToneOffset: number;
  powerOffset: number;
  channelSize: number;
  numberChannels: number;
  radioSettingsSchemaPath: string;
  channelSettingsSchemaPath: string;
}
