// node dist/debug/import.js <port> <file>

import { RadioProgressIndicator } from '@springfield/ham-radio-api';
import { SerialPort } from 'serialport';
import UV5R from '../models/uv5r.js';
import winston from 'winston';
import fs from 'node:fs';
import { LoggerLibrary, LoggerType, LogLayer } from 'loglayer';
import { serializeError } from 'serialize-error';

const winstonLogger = winston.createLogger({
  level: 'debug',
  transports: [new winston.transports.Console()],
  format: winston.format.simple(),
});

const logger = new LogLayer<winston.Logger>({
  logger: {
    instance: winstonLogger as unknown as LoggerLibrary,
    type: LoggerType.WINSTON,
  },
  error: {
    serializer: serializeError,
  },
});

const model = new UV5R('test', logger);

class ConsoleProgressIndicator implements RadioProgressIndicator {
  setValue(value: number): void {
    if (value != 0 && value != 1) {
      // process.stdout.write('.');
    }

    if (value == 1) {
      // process.stdout.write('\n');
    }
  }

  isCanceled: boolean = false;
}

function reset(path: string) {
  return new Promise<void>((resolve, reject) => {
    const port = new SerialPort({ path, baudRate: 9600, autoOpen: false });
    port.open((error) => {
      port.close(() => {
        if (error) {
          reject(error);
        } else {
          resolve();
        }
      });
    });
  });
}
async function test() {
  logger.info(model.getModel().name);
  const serialPortPath = process.argv[2];

  logger.withMetadata({ serialPortPath }).info('Resetting port');
  await reset(serialPortPath);

  logger.withMetadata({ serialPortPath }).info('Importing from radio');
  const memory = await model.getDriver().readRadio(serialPortPath, new ConsoleProgressIndicator());

  if (memory == undefined) {
    logger.info('Canceled');
    return;
  }

  const program = model.getDriver().decodeMemory(memory);

  if (program != undefined) {
    console.table(program.channels.map((programmedChannel) => programmedChannel.radioChannel));

    if (process.argv[3]) {
      fs.writeFileSync(process.argv[3], JSON.stringify(program, null, 2));
    }
  } else {
    logger.withMetadata({ serialPortPath }).info('Canceled!');
  }
}

test();
