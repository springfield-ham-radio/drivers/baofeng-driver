import { RadioProgressIndicator } from '@springfield/ham-radio-api';
import UV5R from '../models/uv5r.js';
import winston from 'winston';
import fs from 'fs';
import { LoggerLibrary, LoggerType, LogLayer } from 'loglayer';
import { serializeError } from 'serialize-error';

const winstonLogger = winston.createLogger({
  level: 'debug',
  transports: [new winston.transports.Console()],
  format: winston.format.simple(),
});

const logger = new LogLayer<winston.Logger>({
  logger: {
    instance: winstonLogger as unknown as LoggerLibrary,
    type: LoggerType.WINSTON,
  },
  error: {
    serializer: serializeError,
  },
});

const model = new UV5R('test', logger);

class NoOpProgressIndicator implements RadioProgressIndicator {
  setValue(): void {}

  isCanceled: boolean = false;
}

async function test() {
  const serialPortPath = process.argv[2];
  const data = JSON.parse(fs.readFileSync(process.argv[3]).toString());

  try {
    const memory = model.getDriver().encodeProgram(data.program, data.memory);
    await model.getDriver().writeRadio(serialPortPath, memory, new NoOpProgressIndicator());
  } catch (error) {
    console.log(error);
  }
}

test();
