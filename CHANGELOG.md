## [5.0.4](https://gitlab.com/springfield-ham-radio/baofeng-driver/compare/v5.0.3...v5.0.4) (2025-02-07)


### Bug Fixes

* **deps:** update commitlint monorepo to ^19.7.1 ([f36e846](https://gitlab.com/springfield-ham-radio/baofeng-driver/commit/f36e84602174ca6af0ad4af3e0621c8044183b7b))

## [5.0.3](https://gitlab.com/springfield-ham-radio/baofeng-driver/compare/v5.0.2...v5.0.3) (2025-01-17)


### Bug Fixes

* **deps:** update dependency @semantic-release/gitlab to ^13.2.4 ([b1660cc](https://gitlab.com/springfield-ham-radio/baofeng-driver/commit/b1660cc4c0f8f3d4af22a9df13cd68359139f8e9))

## [5.0.2](https://gitlab.com/springfield-ham-radio/baofeng-driver/compare/v5.0.1...v5.0.2) (2024-12-20)


### Bug Fixes

* **deps:** update dependency @commitlint/cli to ^19.6.1 ([fa0e27a](https://gitlab.com/springfield-ham-radio/baofeng-driver/commit/fa0e27a2c20839b6bb0e15d329bac600fa4925db))

## [5.0.1](https://gitlab.com/springfield-ham-radio/baofeng-driver/compare/v5.0.0...v5.0.1) (2024-12-06)


### Bug Fixes

* **deps:** update dependency @semantic-release/gitlab to ^13.2.3 ([5cc6eef](https://gitlab.com/springfield-ham-radio/baofeng-driver/commit/5cc6eefb0d4cbf81df254edfe703e17e77f81aa6))

## [5.0.0](https://gitlab.com/springfield-ham-radio/baofeng-driver/compare/v4.0.0...v5.0.0) (2024-12-02)


### ⚠ BREAKING CHANGES

* switch to loglayer

### Features

* switch to loglayer ([19452ac](https://gitlab.com/springfield-ham-radio/baofeng-driver/commit/19452acce3bffcfe5e59f42cb02b43e30bb39a50))


### Bug Fixes

* get import working ([5bd1015](https://gitlab.com/springfield-ham-radio/baofeng-driver/commit/5bd1015a208422504a17c9d464bf2d71fbd68f27))

## [4.0.0](https://gitlab.com/springfield-ham-radio/baofeng-driver/compare/v3.0.0...v4.0.0) (2024-12-01)


### ⚠ BREAKING CHANGES

* force release

### Features

* force release ([2b092ef](https://gitlab.com/springfield-ham-radio/baofeng-driver/commit/2b092efecd33f5c733f64f2dda5f7b8ae9a5e7d2))

## [3.0.0](https://gitlab.com/springfield-ham-radio/baofeng-driver/compare/v2.0.0...v3.0.0) (2024-12-01)


### ⚠ BREAKING CHANGES

* force release

### Features

* force release ([675a501](https://gitlab.com/springfield-ham-radio/baofeng-driver/commit/675a501c30b9ef2cc4cff8625c498c1959351478))

## [2.0.0](https://gitlab.com/springfield-ham-radio/baofeng-driver/compare/v1.1.0...v2.0.0) (2024-12-01)


### ⚠ BREAKING CHANGES

* force release

### Features

* force release ([9a7470d](https://gitlab.com/springfield-ham-radio/baofeng-driver/commit/9a7470d38b6f1ed68ac5d9937d345c1dd9356686))

## [1.1.0](https://gitlab.com/springfield-ham-radio/baofeng-driver/compare/v1.0.0...v1.1.0) (2024-12-01)


### Features

* fix version ([930138a](https://gitlab.com/springfield-ham-radio/baofeng-driver/commit/930138a848a91b0c918cbcffa5ed7e4ad4ee0601))

## 3.3.2 (2024-12-01)


### Features

* improved radio driver ([bbc6cb1](https://gitlab.com/springfield-ham-radio/baofeng-driver/commit/bbc6cb1200fd2c4681f8349aa978d1bc69894c76))
